﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace trie9
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Trie trie = new Trie();

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void открытьФайлToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                StreamReader f_in = new StreamReader(openFileDialog1.FileName);
                string txt = f_in.ReadToEnd();

                string[] words = txt.Split(new[] { ' ', ',', ':', '?', '!' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string word in words)
                {
                    trie.Add(word.ToUpper());
                };

                trie.MakeTree(treeView1);
            }
        }

        private void выполнитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox2.Text = "";
            trie.PrintTree(textBox2);
        }

        private void условиеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Дан текстовый файл, состоящий из слов, возможно, повторяющихся. Распечатать их в алфавитном порядке, указав число вхождений каждого слова в текст", "Условие задачи 9");
        }

        private void добавитьСловоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 addForm = new Form2();
            addForm.ShowDialog();
            if (!string.IsNullOrEmpty(addForm.word))
            {
                trie.Add(addForm.word.Trim().ToUpper());
                trie.MakeTree(treeView1);
            }
        }

        private void удалитьСловоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 deleteForm = new Form2();
            deleteForm.ShowDialog();
            if (!string.IsNullOrEmpty(deleteForm.word))
            {
                if (trie.Delete(deleteForm.word.Trim().ToUpper()))
                {
                    trie.MakeTree(treeView1);
                }
                else
                {
                    MessageBox.Show("Слово не найдено", "Ошибка");
                }
            }
        }

        private void очиститьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            trie.Clear();
            trie.MakeTree(treeView1);
        }
    }
}
